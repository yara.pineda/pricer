const express = require('express')
const path = require('path')
const server = express()
const router = express.Router()
const port = process.env.PORT || 3000;
const mysql = require("mysql")

var conex = initializeConnection({
    host: 'bwlaahqvtg3eeyqcvnic-mysql.services.clever-cloud.com',
    user: 'uog3ytdswafjxrjt',
    password: 'b7xJo0Kw7ZKxHDtf4skM',
    database: 'bwlaahqvtg3eeyqcvnic',
    port: 3306
});

/* var conex = initializeConnection({
    host: 'bqxqbfeb8f4xles7gzwb-mysql.services.clever-cloud.com',
    user: 'uinwno6wk1tnrvm2',
    password: 'HmGICfv3DCtgZHV4W6VO ',
    database: 'bqxqbfeb8f4xles7gzwb',
    port: 3306
}); */

function initializeConnection(config) {
    function addDisconnectHandler(connection) {
        connection.on("error", function (error) {
            if (error instanceof Error) {
                if (error.code === "PROTOCOL_CONNECTION_LOST") {
                    console.error(error.stack);
                    console.log("Lost connection. Reconnecting...");

                    initializeConnection(connection.config);
                } else if (error.fatal) {
                    throw error;
                }
            }
        });
    }

    if (conex) conex.destroy()

    var connection = mysql.createConnection(config);

    // Add handlers.
    addDisconnectHandler(connection);

    connection.connect();
    return connection;
}

router.get('/', function(req, res){
    res.sendFile(path.join(__dirname, 'index.html'))
    console.log("\nRequest of \'/\'");
    console.log("Respondiendo con: index.html");
})

router.get('/save_data', function(req, res){
    var sql = "insert into trades (cliente, accepted, spread, size, coin) values ?"
    var data = req.query
    var accepted = (data.accepted == 'true')
    var values = [ 
        [data.client.toString(), 
        accepted,
        parseFloat(data.spread),
        parseInt(data.size),
        data.coin]
    ]
    conex.query(sql, [values], function(err, result){
        if(err){
            if(err.code === "ER_NO_REFERENCED_ROW"){
                res.status(200).json({error: true, message: "Client not found"})
            } else {
                console.error("Error: " + err)
                res.status(200).json({error: true, message: "Conection Error"})
            }
        } else {
            res.status(200).json(result)
        }
    })
})

router.get('/hit_rate', function(req, res){
    var sql = "select accepted \
        from trades \
        where cliente = ? \
        and coin = ? \
        and spread = ? \
        and size = ?"
    var data = req.query
    console.log(data);
    var values = [ 
        data.client, 
        data.coin,
        parseFloat(data.spread),
        parseInt(data.size),
    ]
    conex.query(sql, values, function(err, result){
        if(err){
            if(err.code === "ER_NO_REFERENCED_ROW"){
                res.status(200).json({error: true, message: "Client not found"})
            } else {
                console.error("Error: " + err)
                res.status(200).json({error: true, message: "Conection Error"})
            }
        } else {
            res.status(200).json(result)
        }
    })
})

server.use('/', router)
// ! Acceder a archivos estáticos
server.use(express.static(path.join(__dirname, 'public')))

server.listen(port, function(){
    console.log("El servidor, vía express, está en ejecuición en el puerto" + port);
    console.log("Terminar con Ctrl + C");
})