function hit_rate(){
    var spread = (parseFloat($("#Spread").val()) * 10000).toFixed(2)
    var data_to_evaluate = {
        client: $("#id_cliente").val(),
        coin: $("#coin").val(),
        spread: spread,
        size: $("#size").val(),
    }
    console.log(data_to_evaluate);
    $.get("/hit_rate", data_to_evaluate, function(data){
        var a = 0, r = 0
        if(data.error)  {
            alert(data.message)
        } else {
            data.forEach(element => {
                if(element.accepted == 1){
                    a += 1
                }else if(element.accepted == 0){
                    r += 1
                }
            });
            if(a == 0 && r == 0){
                alert("no hay información suficiente del cliente")
            } else {
                var hitRate = a / (a + r)
                $("#lbHR").addClass("active")
                $("#Hit_Rate").val(hitRate)
            }
        }
    })
}

function save_data(accepted){
    var spread = (parseFloat($("#Spread").val()) * 10000).toFixed(2)
    console.log("Spread: " + spread);
    var data_to_sent = {
        client: $("#id_cliente").val(),
        accepted: accepted,
        spread: spread,
        size: $("#size").val(),
        coin: $("#coin").val()
    }
    console.log("Data to sent: " + data_to_sent);
    $.get("/save_data", data_to_sent, function(data){
        console.log(data);
        if(data.error)  {
            alert(data.message)
        } else {
            alert("información guardada correctamente")
            if(accepted){
                $("#trade_status").removeClass("yellow")
                $("#trade_status").removeClass("red")
                $("#trade_status").addClass("green")
                $("#trade_status").text("Trade accepted and data saved")
                alert(`You ${($("#OSL_side").val() == "sell") ? "sold: " : "bought: "} ${$("#size").val()} at ${$("#price").val()}`)
            } else {
                $("#trade_status").removeClass("yellow")
                $("#trade_status").removeClass("green")
                $("#trade_status").addClass("red")
                $("#trade_status").text("Trade rejected and data saved")
            }
        }
    })
}